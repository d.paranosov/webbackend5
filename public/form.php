<!DOCTYPE html>

<html lang="ru">

    <head>
        <meta charset="utf-8">
        <title>Задание 5</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <style>
        .error {
            border: 2px solid red;
        }
        .exception {
            border: 2px solid orange;
        }
        </style>
    </head>

    <body>

        <div id="main-wrapper">
        <header>
            <a id="my_heading"></a>
            <img class="title1" src="img/image1.png" alt="Описание картинки"/>
            <div class="name">Задание 5</div>

        </header>

        <nav id="my_nav">
            <div><a href="#my_nav" title="Меню сайта">Меню</a></div>
            <div><a href="#my_heading" title="Заголовок сайта">Заголовок</a></div>
            <div><a href="#hyperlinks" title="Первая часть задания">Гиперссылки</a></div>
            <div><a href="#my_table" title="Новая часть">Таблица</a></div>
            <div><a href="#my_form" title="Вторая часть задания">Форма</a></div>
            <div><a href="#my_footer" title="Подвал сайта">Подвал</a></div>
            
        </nav>

        <div class="links">
            <a id="hyperlinks"></a>
            <ol>
                <li><a href="http://example.com">Ссылка на example.com</a></li>
                <li><a href="https://example.com">Ссылка на example.com в протоколе https</a></li>
                <li><a href="ftp://ftp-servers.example.com/Temp/file.txt">Ссылка на файл на сервере FTP без авторизации</a></li>
                <li><a href="ftp://user:pass123@ftp-servers.example.com/Temp/file.txt">Ссылка на файл на сервере FTP с авторизацией</a></li>
                <li><a href="http://example.com/page#about">Ссылка на фрагмент страницы некоторого сайта</a></li>
                <li><a href="#end">Ссылка на фрагмент текущей страницы</a></li>
                <li><a href="https://www.onlinestor.com?product=pens&color=blue">Ссылка с двумя параметрами в URL</a></li>
                <li>
                    <ul>
                        <li><a href="#hyperlinks" title="Первая часть задания">Гиперссылки</a></li>
                        <li><a href="#my_form" title="Вторая часть задания">Форма</a></li>
                    </ul>
                </li>
                <li><a title="Ссылка без href">https://example.com</a></li>
                <li><a href="">Ссылка с пустым href</a></li>
                <li><a href="http://example.com" rel="nofollow">Ссылка, по которой запрещен переход поисковикам</a></li>
                <li><!--noindex--><a href="http://example.com">Запрещенная для индексации поисковиками</a><!--/noindex--></li>
                <li><p>Контекстная <a href="http://example.com">гиперссылка</a> в тексте абзаца</p></li>
                <li><a href="http://example.com"><img src="img/image.jpg" width="400" 
                    height="300" alt="Пример"></a></li>
                <li><img src = "img/image.jpg" width="220" alt="Пример" 
                    height="165" usemap = "#map">
                    <map name = "map">
                    <area shape = "rect" href = "http://example.com" alt="Прямоугольник" coords = "20,20,120,120"/>
                    <area shape = "circle" href = "http://example.com" alt="Круг" coords = "180,70,40"/>
                    </map>
                </li>
                <li><a href="demo.html">Относительная на страницу в текущем каталоге</a></li>
                <li><a href="about/form.html">Относительная на страницу в каталоге about</a></li>
                <li><a href="../form.html">Относительная на страницу в каталоге уровнем выше текущего</a></li>
                <li><a href="../../demo.html">Относительная на страницу в каталоге двумя уровнями выше</a></li>
                <li><a href="/level_above/my_html/index.html">Сокращенная на главную</a></li>
                <li><a href="/level_above/my_html/about/form.html">Сокращенная ссылка на внутреннюю</a></li>
            </ol>
        </div>

        <div class="table">
        <table>
            <caption><a id="my_table"></a>Представители подотряда "собакообразные"</caption>
            <tr class="dark">
              <th>Псовые</th>
              <th>Медвежьи</th>
              <th>Енотовые</th>
              <th>Скунсовые</th>
            </tr>
            <tr class="light">
              <td colspan="4">Относятся к отряду "хищные"</td>
            </tr>
            <tr class="dark">
              <td>Бульдог</td>
              <td>Барибал</td>
              <td>Енот-ракоед</td>
              <td>Малый скунс</td>
            </tr>
            <tr class="light">
                <td>Волк</td>
                <td>Белый медведь</td>
                <td>Коати</td>
                <td>Полосатый скунс</td>
            </tr>
            <tr class="dark">
                <td>Рыжая лисица</td>
                <td>Малайский медведь</td>
                <td>Косумельский енот</td>
                <td>Мексиканский скунс</td>
            </tr>
            <tr class="light">
                <td>Койот</td>
                <td>Бурый медведь</td>
                <td>Кинкажу</td>
                <td>Полуполосый скунс</td>
            </tr>
          </table>
        </div>

        <?php
            if (!empty($messages)) {
                print('<div id="messages">');
                // Выводим все сообщения.
                foreach ($messages as $message) {
                    print($message);
                }
                print('</div>');
            }
            ?>


        <form action="" method="POST">
            
            <a id="my_form"></a>
            <div class="form__item">
            <label>
                Имя:<br />
                <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> <?php if ($exceptions['fio']) {print 'class="exception"';} ?> value="<?php print $values['fio']; ?>" type="text" />
            </label>
            </div>

            <div class="form__item">
            <label>
                E-mail:<br />
                <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> <?php if ($exceptions['email']) {print 'class="exception"';} ?> value="<?php print $values['email']; ?>" type="email" />
            </label>
            </div>

            <div class="form__item">
            <label>
                Дата рождения:<br />
                <input name="date" min="1900-01-01" max="2021-04-11" <?php if ($errors['date']) {print 'class="error"';} ?> <?php if ($exceptions['date']) {print 'class="exception"';} ?> value='<?php print $values['date']; ?>' type="date" />
            </label>
            </div>

            <div class="form__item">
            Пол:<br />
            <label>
                <input type="radio" id="radio1"
                name="radio-group-1" <?php if($values['radio-group-1'] == 'M') {print 'checked';} else{print "";} ?> value="M" />
                Мужской
            </label>
            <label>
                <input type="radio" id="radio2"
                name="radio-group-1" <?php if($values['radio-group-1'] == 'W') {print 'checked';} else{print "";} ?> value="W" />
                Женский
            </label>
            </div>

            <div class="form__item">
            Количество конечностей:<br />
            <label>
                <input type="radio" <?php if($values['radio-group-2'] == '1') {print 'checked';} else{print "";} ?>
                name="radio-group-2" value=1 />
                Одна
            </label>
            <label>
                <input type="radio" <?php if($values['radio-group-2'] == '2') {print 'checked';} else{print "";} ?>
                name="radio-group-2" value=2 />
                Две
            </label>
            <label>
                <input type="radio" <?php if($values['radio-group-2'] == '3') {print 'checked';} else{print "";} ?>
                name="radio-group-2" value=3 />
                Три
            </label>
            <label>
                <input type="radio" <?php if($values['radio-group-2'] == '4') {print 'checked';} else{print "";} ?>
                name="radio-group-2" value=4 />
                Четыре
            </label>
            </div>

            <div class="form__item">
            Сверхспособности:<br />
            <label>
                <select name="field-name-4[]"
                    multiple="multiple">
                <option <?php if($values['field-name-4'][0] == '1') {print 'selected';} else{print "";} ?> value=0>Бессмертие</option>
                <option <?php if($values['field-name-4'][1] == '1') {print 'selected';} else{print "";} ?> value=1>Прохождение сквозь стены</option>
                <option <?php if($values['field-name-4'][2] == '1') {print 'selected';} else{print "";} ?> value=2>Левитация</option>
                </select>
            </label>
            </div>

            <div class="form__item">
            <label>
                Биография:<br />
                <textarea name="field-name-2" <?php if ($errors['field-name-2']) {print 'class="error"';} ?> <?php if ($exceptions['field-name-2']) {print 'class="exception"';} ?> ><?php print $values['field-name-2']; ?></textarea>
            </label>
            </div>

            <div class="form__item">
            <a id="end"></a>С контрактом ознакомлен
            <label><input type="checkbox" 
                name="check-1" <?php if ($errors['check-1']) {print 'class="error"';} ?>
                <?php if($values['check-1'] == 'ok') {print 'checked';} else{print "";} ?> value="ok" />
            </label>
            </div>

            <div class="form__item">
                <input name="submit" type="submit" value="Отправить" />
            </div>

            <?php
            if(!empty($_SESSION['login'])) {
            ?>
                <div class="form__item">
                    <input name="button" onclick="<?php session_destroy();?>" type="button" value="Выйти из сессии" />
                </div>

            <?php
            } else {
            ?>
                <div class="form__item">
                    <a href="login.php">Изменить информацию</a>
                </div>
            <?php
            }
            ?>
        </form>

        <footer>
            <a id="my_footer"></a>
            (с) Евгений Параносов 2020
        </footer>
        </div>

    </body>

</html>

