<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

<form action="" method="post">
  <input name="login" />
  <input name="pass" />
  <input type="submit" value="Войти" />
</form>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
    $user_login = (string)$_POST['login'];
    $user_pass = md5($_POST['pass']);

    $link = mysqli_connect("localhost", "u21391", "5188469", "u21391");
    /* проверка соединения */
    if (mysqli_connect_errno()) {
        printf("Не удалось подключиться: %s\n", mysqli_connect_error());
        exit();
    }

    $query = mysqli_query($link, "SELECT id_user, login, password FROM registered WHERE login='$user_login'");
    $row = mysqli_fetch_array($query, MYSQLI_ASSOC);

    //$result = mysqli_query($link, "SELECT id_user FROM registered WHERE login='$user_login'");
    if($user_login == $row["login"] && $user_pass == $row["password"]) {
        // Если все ок, то авторизуем пользователя.
        $_SESSION['login'] = $user_login;
        // Записываем ID пользователя.
        $_SESSION['uid'] = $row["id_user"];
        mysqli_close($link);
        echo "Вы в системе";

    } else {
        echo "Вы не зарегистрированы";
    }



  // Делаем перенаправление.
  header('Location: ./');
}
